var express = require('express');
var mongodb = require('mongodb');
var mongoclient = mongodb.MongoClient;
var router = express.Router();
require('dotenv').config();
var url = process.env.DBURL;

var options = {
    replSet: {
        sslValidate: false
    }
}

router.get('/', (req, res) => {
    mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
        if (error) throw error;
        let dbObject = db.db('clinicdb');

        dbObject.collection('appointments').find().toArray((error, result) => {
            if (error) throw error;
            var allAppointments = JSON.stringify(result);

            dbObject.collection('patients').find().toArray((error, result) => {
                if (error) throw error;
                var allPatients = JSON.stringify(result);

                dbObject.collection('doctors').find().toArray((error, result) => {
                    if (error) throw error;
                    db.close();

                    res.send({
                        appointments: allAppointments,
                        patients: allPatients,
                        doctors: JSON.stringify(result)
                    })
                })
            })
        })
    });
});

router.get('/finddates', (req, res) => {
  let form = req.body;
  mongoclient.connect(url, options, {useUnifiedTopology: true}, (error, db) => {
    if(error) throw error;
    let dbObject = db.db('clinicdb');

    dbObject.collection('appointments').findOne(form, (error, result) => {
      if(error) throw error;
      res.send(JSON.stringify(result));
    });
  })
});

router.post('/savedate', (req, res) => {
    let form = req.body;
    mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
        if (error) throw error;
      let dbObject = db.db('clinicdb');
  
        dbObject.collection('appointments').insertOne(form, (error, result) => {
          if (error) throw error;
          db.close();
          res.end();
        })
    })
  });
  
  router.put('/editdate/:id', (req, res) => {
      let form = {$set: req.body};
      let id = { _id: new mongodb.ObjectID(req.params.id) };
      mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
        if (error) throw error;
        let dbObject = db.db('clinicdb');
  
        dbObject.collection('appointments').updateOne(id, form, (error, result) => {
          if (error) throw error;
          db.close();
          res.end();
        })
      })
  });
  
  router.delete('/deletedate/:id', (req, res) => {
    let id = { _id: new mongodb.ObjectID(req.params.id) };
    mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
      if (error) throw error;
      let dbObject = db.db('clinicdb');
  
      dbObject.collection('appointments').deleteOne(id, (error, result) => {
        if (error) throw error;
        db.close();
        res.end();
      })
    })
  });

module.exports = router;