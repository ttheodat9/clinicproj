var express = require('express');
var mongodb = require('mongodb');
var mongoclient = mongodb.MongoClient;
var router = express.Router();
require('dotenv').config();
var url = process.env.DBURL;

var options = {
   replSet: {
     sslValidate: false
   }
  }

/* GET home page. */
router.get('/', function (req, res, next) {
  mongoclient.connect(url + '/mymongodb', options, { useUnifiedTopology: true }, (error, db) => {
      if (error) throw error;
      let dbObject = db.db('clinicdb');

      dbObject.collection('administration').find().toArray((error, result) => {
        if(error) throw error;
        db.close();
        res.send(JSON.stringify(result));

      })
  });
});
router.get('/findadmin', (req, res) => {
  let form = req.body;
  mongoclient.connect(url, options, {useUnifiedTopology: true}, (error, db) => {
    if(error) throw error;
    let dbObject = db.db('clinicdb');

    dbObject.collection('admin').findOne(form, (error, result) => {
      if(error) throw error;
      res.send(JSON.stringify(result));
    });
  })
});
router.post('/saveadmin', (req, res) => {
    let form = req.body;
    console.log(form);
    mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
        if (error) throw error;
      let dbObject = db.db('clinicdb');
  
        dbObject.collection('administration').insertOne(form, (error, result) => {
          if (error) throw error;
          db.close();
          res.end();
        })
    })
  });
  
  router.put('/editadmin/:id', (req, res) => {
      let form = {$set: req.body};
      let id = { _id: new mongodb.ObjectID(req.params.id) };
      mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
        if (error) throw error;
        let dbObject = db.db('clinicdb');
  
        dbObject.collection('administration').updateOne(id, form, (error, result) => {
          if (error) throw error;
          db.close();
          res.end();
        })
      })
  });
  
  router.delete('/deleteadmin/:id', (req, res) => {
    let id = { _id: new mongodb.ObjectID(req.params.id) };
    mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
      if (error) throw error;
      let dbObject = db.db('clinicdb');
  
      dbObject.collection('administration').deleteOne(id, (error, result) => {
        if (error) throw error;
        db.close();
        res.end();
      })
    })
  });

module.exports = router;
