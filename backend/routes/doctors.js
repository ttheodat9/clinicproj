var express = require('express');
var mongodb = require('mongodb');
var mongoclient = mongodb.MongoClient;
var router = express.Router();
require('dotenv').config();
var url = process.env.DBURL;

var options = {
  replSet: {
    sslValidate: false
  }
}

router.get('/', function (req, res, next) {
  mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
    if (error) throw error;
    let dbObject = db.db('clinicdb');

    dbObject.collection('doctors').find().toArray((err, result) => {
      if (err) throw err;
      db.close();
      res.end(JSON.stringify(result));
    })
  });
});
router.get('/finddoctors', (req, res) => {
  let form = req.body;
  mongoclient.connect(url, options, {useUnifiedTopology: true}, (error, db) => {
    if(error) throw error;
    let dbObject = db.db('clinicdb');

    dbObject.collection('doctors').findOne(form, (error, result) => {
      if(error) throw error;
      res.send(JSON.stringify(result));
    });
  })
});

router.post('/savedoctor', (req, res) => {
  let form = req.body;
  console.log(form);
  mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
      if (error) throw error;
    let dbObject = db.db('clinicdb');

      dbObject.collection('doctors').insertOne(form, (error, result) => {
        if (error) throw error;
        db.close();
        res.end();
      })
  })
});

router.put('/editdoctor/:id', (req, res) => {
    let form = {$set: req.body};
    let id = { _id: new mongodb.ObjectID(req.params.id) };
    mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
      if (error) throw error;
      let dbObject = db.db('clinicdb');

      dbObject.collection('doctors').updateOne(id, form, (error, result) => {
        if (error) throw error;
        db.close();
        res.end();
      })
    })
});

router.delete('/deletedoctor/:id', (req, res) => {
  let id = { _id: new mongodb.ObjectID(req.params.id) };
  mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
    if (error) throw error;
    let dbObject = db.db('clinicdb');

    dbObject.collection('doctors').deleteOne(id, (error, result) => {
      if (error) throw error;
      db.close();
      res.end();
    })
  })
});

module.exports = router;