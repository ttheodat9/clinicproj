var express = require('express');
var mongodb = require('mongodb');
var mongoclient = mongodb.MongoClient;
var router = express.Router();
require('dotenv').config();
var url = process.env.DBURL;

var options = {
    replSet: {
        sslValidate: false
    }
}

router.get('/', (req, res) => {
    mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
        if (error) throw error;
        let dbObject = db.db('clinicdb');

        dbObject.collection('patients').find().toArray((err, result) => {
            if (error) throw error;
            db.close();
            res.end(JSON.stringify(result));
        })
    });
});
router.get('/findpatients', (req, res) => {
  let form = req.body;
  mongoclient.connect(url, options, {useUnifiedTopology: true}, (error, db) => {
    if(error) throw error;
    let dbObject = db.db('clinicdb');

    dbObject.collection('patients').findOne(form, (error, result) => {
      if(error) throw error;
      res.send(JSON.stringify(result));
    });
  })
});

router.post('/savepatient', (req, res) => {
    let form = req.body;
    mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
        if (error) throw error;
      let dbObject = db.db('clinicdb');
  
        dbObject.collection('patients').insertOne(form, (error, result) => {
          if (error) throw error;
          db.close();
          res.end();
        })
    })
  });
  
  router.put('/editpatient/:id', (req, res) => {
      let form = {$set: req.body};
      let id = { _id: new mongodb.ObjectID(req.params.id) };
      mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
        if (error) throw error;
        let dbObject = db.db('clinicdb');
  
        dbObject.collection('patients').updateOne(id, form, (error, result) => {
          if (error) throw error;
          db.close();
          res.end();
        })
      })
  });
  
  router.delete('/deletepatient/:id', (req, res) => {
    let id = { _id: new mongodb.ObjectID(req.params.id) };
    mongoclient.connect(url, options, { useUnifiedTopology: true }, (error, db) => {
      if (error) throw error;
      let dbObject = db.db('clinicdb');
  
      dbObject.collection('patients').deleteOne(id, (error, result) => {
        if (error) throw error;
        db.close();
        res.end();
      })
    })
  });

module.exports = router;