import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/index.vue'
import ManageAdmin from '../views/manageAdmin.vue'
import Admin from '../views/admin.vue'
import Staff from '../views/staff.vue'
import ManageStaff from '../views/manageStaff.vue'
import Doctors from '../views/doctors.vue'
import Patients from '../views/patients.vue'
import Appointments from '../views/appointments.vue'



//Used to define the base URL of all AJAX requests to the backend
//This is the URL where the Express server is listening for requests
//Made global to be used by the whole application
global.baseURL = 'http://localhost:1800'; 

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/admin',
    name: 'Admin',
    component: Admin
  },
  {
    path: '/manageAdmin',
    name: 'ManageAdmin',
    component: ManageAdmin
  },
  {
    path: '/staff',
    name: 'Staff',
    component: Staff
  },
  {
    path: '/manageStaff',
    name: 'ManageStaff',
    component: ManageStaff
  },
  {
    path: '/doctors',
    name: 'Doctors',
    component: Doctors
  },
  {
    path: '/patients',
    name: 'Patients',
    component: Patients
  },
  {
    path: '/appointments',
    name: 'Appointments',
    component: Appointments
  }
]

const router = new VueRouter({
  routes
})

export default router


